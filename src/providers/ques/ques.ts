// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the QuesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class QuesProvider {
  selectQues: any = [];
  constructor() {}

  getSelectedQuestion() {
    return this.selectQues;
  }

  addQuestions(question: any) {
    this.selectQues.push(question);
    // console.log(question);
  }

  onEnable(index1: number) {
    // var isSelected = true;
    // var isChoosed = true;
  }
}
