import { Http } from '@angular/http';
import { AppRate } from '@ionic-native/app-rate';
// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

/*
  Generated class for the RatingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RatingProvider {
  appRate: any = AppRate;

  constructor(
    public http: Http,
    private appRate1: AppRate,
    public platform: Platform
  ) {
    this.appRate.preferences = {
      displayAppName: 'Ionic Rating',
      storeAppURL: {
        //  ios                 : AppID,
        android: 'market://details?id=com.ionic.viewapp'
      }
    };

    this.appRate.preferences = {
      displayAppName: 'Testbook',
      usesUntilPrompt: 4,
      promptAgainForEachNewVersion: true,
      storeAppURL: {
        // ios: '<app_id>',
        android: 'market://details?id=<io.ionic.testBook>'
      },
      customLocale: {
        title: 'Do you enjoy %@?',
        message: 'if you enjoy %@?.would you mind talking to rate it?',
        cancelButtonLabel: 'No, Thanks',
        laterButtonLabel: 'Remind me later',
        rateButtonLabel: 'Rate It Now'
      },
      callbacks: {
        onRateDialogShow: function(callbacks) {
          console.log('');
        },
        onButtonClicked: function(buttonIndex) {
          console.log('selected index:', buttonIndex);
        }
      }
    };
  }

  requestRating() {
    this.appRate1.promptForRating(true);
  }
}
