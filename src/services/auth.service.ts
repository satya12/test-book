import { SlidesPage } from './../pages/slides/slides';
import { Injectable } from '@angular/core';
import { AlertController, App } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { Http, Headers } from '@angular/http';

@Injectable()
export class AuthService {
  authToken: any;
  user: any;
  apiUrl: string = 'https://candidate-cfe.herokuapp.com';
  constructor(
    private alrtCtrl: AlertController,
    private http: Http,
    private app: App,
    private storage: Storage
  ) {}

  _errorHandler(error: any) {
    if (error.status == 0) {
      console.error(error);
      return Observable.throw({
        _body: JSON.stringify({ msg: 'Server Not responding!!' })
      });
    } else if (error.status > 400 && error.status < 500) {
      return Observable.throw(
        error || { _body: JSON.stringify({ msg: 'Something went wrong!!' }) }
      );
    }
    return Observable.throw(error || 'Server Error!!');
  }

  //===========================================
  ///             ON LOGOUT
  //===========================================
  logOut() {
    return new Promise((resolve, reject) => {
      this.alrtCtrl
        .create({
          title: 'Log out ?',
          message: 'Are you sure to logout ?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                reject();
              }
            },
            {
              text: 'Confirm',
              handler: () => {
                this.storage.clear();
                // .then(() => {
                // this.app.getRootNav().setRoot(SlidesPage);
                // });
                // window.localStorage.clear();
                resolve();
              }
            }
          ]
        })
        .present();
    });
  }
  //===========================================
  ///             ON LOGIN
  //===========================================
  logIn(user: any) {
    // authenticateUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
      .post(this.apiUrl + '/users/authenticate', user, { headers: headers })
      .map(res => {
        let data = res.json();
        return data;
      })
      .catch(this._errorHandler);
  }

  //===========================================
  // Storing token in local storage
  //===========================================
  authSuccess(token, user) {
    this.storage.set('token', token);
    this.storage.set('user', user);
  }

  //===========================================
  ///            Register
  //===========================================
  signUp(data: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http
      .post(this.apiUrl + '/users/register', data, { headers: headers })
      .map(res => res.json())
      .catch(this._errorHandler);
  }

  // storeUserData(token, user) {
  //   localStorage.setItem('id_token', token);
  //   // console.log("heyyyy tokennnn issss coming", token);
  //   localStorage.setItem('user', JSON.stringify(user));
  //   this.authToken = token;
  //   this.user = user;
  //   console.log('userdata', this.user);
  // }
}
