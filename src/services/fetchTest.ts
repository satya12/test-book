import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class fetchTestService {
  selectedQuestion: any;
  str: string;
  testDetails = {
    subject: ''
  };
  testPDF: any = {};
  private url: string = 'https://candidate-cfe.herokuapp.com/';

  constructor(private http: Http, private storage: Storage) {}

  setHeaders(headers) {
    return this.storage.get('token').then(token => {
      if (token != null) {
        headers.append('Authorization', 'JWT ' + token);
      }
      return headers;
    });
  }

  //----------Getting TestCategory----------------//
  categoryQuestions(search: string, category: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return Observable.fromPromise(this.setHeaders(headers)).switchMap(headers =>
      this.http
        .get(this.url + 'tests/categoryQuestions', {
          headers: headers,
          params: { category: category, search: search }
        })
        .map(res => res.json())
    );
  }

  passToPDF(data: any) {
    this.selectedQuestion = data;
  }

  savePDF(pdfData: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.storage.get('user').then(user => {
      console.log('id user', user);
      this.testPDF = {
        doc: pdfData,
        testDetails: this.testDetails,
        user: user
      };

      return Observable.fromPromise(this.setHeaders(headers)).switchMap(
        headers =>
          this.http
            .post(this.url + 'tests/savepdf', this.testPDF, {
              headers: headers
            })
            .map(res => {
              console.log(res);
              return res;
            })
      );
    });
  }

  getPDFs(userId) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return Observable.fromPromise(this.setHeaders(headers)).switchMap(headers =>
      this.http
        .get(this.url + 'tests/getpdfs', {
          params: { userId: userId },
          headers: headers
        })
        .map((res: Response) => {
          let pdfArray = JSON.parse(res.text());
          return pdfArray;
        })
    );
  }
}
