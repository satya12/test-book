// import { HomePage } from './../pages/home/home';
// import { RatingProvider } from './../providers/rating/rating';

import { LoginPage } from './../pages/login/login';
// import { AppRate } from '@ionic-native/app-rate';
import { NavController, LoadingController, Loading } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SlidesPage } from '../pages/slides/slides';
import { Storage } from '@ionic/storage';
import { DashboardPage } from '../pages/dashboard/dashboard';
// import { TestDetailPage } from '../pages/test-detail/test-detail';

// import jQuery from 'jquery';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  // tabsPage = TabsPage;

  // r = LoginPage;
  loading: Loading;
  showedAlert: boolean;
  confirmAlert: any;
  @ViewChild('nav') nav: NavController;

  constructor(
    private platform: Platform,
    private storage: Storage,
    // private rate: RatingProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {
    this.platform.ready().then(() => {
      // window.localStorage.clear();
      statusBar.styleDefault();
      splashScreen.hide();
      this.showLoading();
      this.storage.get('token').then(loggedIn => {
        console.log('token', loggedIn);
        this.rootPage = loggedIn ? DashboardPage : SlidesPage;
        this.loading.dismiss();
      });

      this.showedAlert = false;
      //-------------------------------------------
      //-------------Confirm App exit------------------
      //-------------------------------------------
      this.platform.registerBackButtonAction(() => {
        if (this.nav.length() == 1) {
          if (!this.showedAlert) {
            this.confirmExitApp();
          } else {
            this.showedAlert = false;
            this.confirmAlert.dismiss();
          }
        }

        this.nav.pop();
      });
    });
  }

  confirmExitApp() {
    this.showedAlert = true;
    this.confirmAlert = this.alertCtrl.create({
      title: 'Exit App',
      message: 'Are you sure to exit from App?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.showedAlert = false;
            return;
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.confirmAlert.present();
  }

  //-------------------------------------------
  //------------- Loader display ------------------
  //-------------------------------------------

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      cssClass: 'loadData'
    });
    this.loading.present();
  }
}
