import { PrivacyPage } from './../pages/privacy/privacy';

import { DashboardPage } from './../pages/dashboard/dashboard';
import { TestDetailPage } from './../pages/test-detail/test-detail';
import { AboutPage } from './../pages/about/about';
import { SlidesPage } from './../pages/slides/slides';
import { PopoverPage } from './../pages/popover/popover';
import { AuthService } from './../services/auth.service';
import { SignupPage } from './../pages/signup/signup';
import { LoginPage } from './../pages/login/login';
import { MyApp } from './app.component';
import { HomePage } from './../pages/home/home';
import { TabsPage } from './../pages/tabs/tabs';
import { SelectedQuestionPage } from './../pages/selected-question/selected-question';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { QuesProvider } from '../providers/ques/ques';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { HttpModule } from '@angular/http';
import { fetchTestService } from '../services/fetchTest';

// rating
import { AppRate } from '@ionic-native/app-rate';
// import { RatingProvider } from '../providers/rating/rating';
import { IonicStorageModule } from '@ionic/storage';
import { RatingProvider } from '../providers/rating/rating';
@NgModule({
  declarations: [
    MyApp,
    SlidesPage,
    HomePage,
    TabsPage,
    DashboardPage,
    SelectedQuestionPage,
    LoginPage,
    SignupPage,
    PopoverPage,
    AboutPage,
    TestDetailPage,
    PrivacyPage
  ],
  imports: [
    BrowserModule,
    // BrowserAnimationsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SlidesPage,
    HomePage,
    TabsPage,
    SelectedQuestionPage,
    LoginPage,
    DashboardPage,
    SignupPage,
    PopoverPage,
    AboutPage,
    TestDetailPage,
    PrivacyPage
  ],
  providers: [
    QuesProvider,
    fetchTestService,
    AppRate,
    AuthService,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    File,
    FileOpener,
    RatingProvider
  ]
})
export class AppModule {}
