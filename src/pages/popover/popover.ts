import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  App
} from 'ionic-angular';
import { AboutPage } from '../about/about';
import { AuthService } from '../../services/auth.service';
import { LoginPage } from '../login/login';
// import{PopoverPage}
/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {
  data: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private authService: AuthService,
    private app: App,
    private storage: Storage
  ) {}

  //----------------------------------------
  //----------Navigate to aboutus page -----
  //----------------------------------------
  onAboutUs() {
    this.viewCtrl.dismiss();
    this.app.getRootNav().push(AboutPage);
  }

  //---------------------
  //----------Logout -----
  //----------------------
  onLogout1() {
    this.viewCtrl.dismiss().then(() => {
      this.authService.logOut().then(
        () => {
          this.app.getRootNav().setRoot(LoginPage);
          this.storage.clear();
        },
        () => {}
      );
    });

    // this.viewCtrl.dismiss();
    // this.navCtrl.setRoot(SlidesPage);
  }
}
