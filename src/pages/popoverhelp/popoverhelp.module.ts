import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverhelpPage } from './popoverhelp';

@NgModule({
  declarations: [
    PopoverhelpPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverhelpPage),
  ],
})
export class PopoverhelpPageModule {}
