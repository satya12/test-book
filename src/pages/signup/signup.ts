import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController
} from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';

import { LoginPage } from '../login/login';
import { AuthService } from '../../services/auth.service';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
  // animations: [
  //   //For the logo
  //   trigger('flyInBottomSlow', [
  //     state(
  //       'in',
  //       style({
  //         transform: 'translate3d(0,0,0)'
  //       })
  //     ),
  //     transition('void => *', [
  //       style({ transform: 'translate3d(0,2000px,0' }),
  //       animate('2000ms ease-in-out')
  //     ])
  //   ]),

  //   //For the background detail
  //   trigger('flyInBottomFast', [
  //     state(
  //       'in',
  //       style({
  //         transform: 'translate3d(0,0,0)'
  //       })
  //     ),
  //     transition('void => *', [
  //       style({ transform: 'translate3d(0,2000px,0)' }),
  //       animate('1000ms ease-in-out')
  //     ])
  //   ]),

  //   //For the login form
  //   trigger('bounceInBottom', [
  //     state(
  //       'in',
  //       style({
  //         transform: 'translate3d(0,0,0)'
  //       })
  //     ),
  //     transition('void => *', [
  //       animate(
  //         '2000ms 200ms ease-in',
  //         keyframes([
  //           style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
  //           style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
  //           style({ transform: 'translate3d(0,0,0)', offset: 1 })
  //         ])
  //       )
  //     ])
  //   ]),

  //   //For login button
  //   trigger('fadeIn', [
  //     state(
  //       'in',
  //       style({
  //         opacity: 1
  //       })
  //     ),
  //     transition('void => *', [
  //       style({ opacity: 0 }),
  //       animate('1000ms 2000ms ease-in')
  //     ])
  //   ])
  // ]
})
export class SignupPage {
  signupform: FormGroup;
  fsubmitted: boolean = false;
  // logoState: any = 'in';
  // cloudState: any = 'in';
  // loginState: any = 'in';
  // formState: any = 'in';

  constructor(
    public navCtrl: NavController,
    private authService: AuthService,
    private loader: LoadingController,
    private toast: ToastController,
    public navParams: NavParams
  ) {
    this.createForm();
  }

  createForm() {
    this.signupform = new FormGroup({
      insName: new FormControl('', Validators.required),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]+$/),
        Validators.minLength(6)
      ])
    });
  }

  navigateToLogin() {
    this.navCtrl.pop();
  }

  //===============================
  //      ON SIGNUP FORM SUBMIT
  //===============================
  onSignUp(form: NgForm) {
    if (form.valid) {
      let loading = this.loader.create({
        content: 'Please Wait...',
        spinner: 'circles',
        duration: 10000
      });
      loading.present();
      this.authService.signUp(form.value).subscribe(
        data => {
          this.toaster(data.msg);
          form.reset();
          loading.dismiss();
          // this.app.getRootNav().setRoot(LoginPage);
          this.navCtrl.push(LoginPage);
        },
        err => {
          loading.dismiss();
          this.toaster(JSON.parse(err._body).msg);
        }
      );
    } else {
      this.fsubmitted = true;
    }
  }
  toaster(message: string) {
    this.toast
      .create({
        message: message,
        duration: 1500
      })
      .present();
  }
}
