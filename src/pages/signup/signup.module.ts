import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [SignupPage],
  imports: [IonicPageModule.forChild(SignupPage), BrowserAnimationsModule]
})
export class SignupPageModule {}
