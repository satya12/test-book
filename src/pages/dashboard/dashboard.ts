import { PrivacyPage } from './../privacy/privacy';
// import { LoginPage } from './../login/login';
import { TestDetailPage } from './../test-detail/test-detail';
import { fetchTestService } from './../../services/fetchTest';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import {
  IonicPage,
  NavController,
  Loading,
  LoadingController,
  PopoverController,
  Platform
} from 'ionic-angular';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
// import { AuthService } from '../../services/auth.service';
import { PopoverPage } from '../popover/popover';
import { RatingProvider } from '../../providers/rating/rating';
// import { SlidesPage } from '../slides/slides';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  showMsg: boolean;
  loading: Loading;
  ishaveTest = false;
  pdfArray: any[] = [];

  constructor(
    private navCtrl: NavController,
    private fetchPro: fetchTestService,
    private storage: Storage,
    // private authService: AuthService,
    // private viewCtrl: ViewController,
    // private app: App,
    private loadingCtrl: LoadingController,
    private popoverCtrl: PopoverController,
    private ratePro: RatingProvider,
    private platform: Platform
  ) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  ionViewDidLoad() {
    this.showLoading();
    this.storage.get('user').then(userInfo => {
      console.log('user storage', userInfo);
      if (userInfo) {
        let userId = userInfo.id;
        this.fetchPro.getPDFs(userId).subscribe(
          res => {
            console.log(res);
            if (res.pdf.length > 0) {
              this.ishaveTest = true;
              this.pdfArray = res.pdf;
              this.showMsg = false;
            } else if (res.samplepdf.length > 0) {
              this.ishaveTest = true;
              this.pdfArray = res.samplepdf;
              this.showMsg = true;
            } else {
              this.ishaveTest = false;
            }
            this.loading.dismiss();
          },
          error => {
            this.loading.dismiss();
            this.ishaveTest = false;
          }
        );
      } else {
        this.loading.dismiss();
        this.ishaveTest = false;
        // this.navCtrl.push(SlidesPage);
      }
    });
  }

  promptForAppRating() {
    this.platform.ready().then(() => {
      console.log('tty');
      // Trigger the App Rate plugin modal window
      this.ratePro.requestRating();
      console.log('Control are here');
    });
  }

  //------------------------------------------
  //------------- Creating popoverController --
  //-------------------------------------------

  showpopdown(newevent) {
    let popover = this.popoverCtrl.create(PopoverPage);

    popover.present({
      ev: newevent
    });
  }

  //------------------------------------------
  //------------- Navigate to TestDetailPage --
  //-------------------------------------------
  onNewTest() {
    this.navCtrl.push(TestDetailPage);
  }

  //------------------------------------------
  //------------- Navigate to PrivacyPage --
  //-------------------------------------------
  onPrivacy() {
    this.navCtrl.push(PrivacyPage);
  }

  //-------------------------------------------
  //------------- Loader display --------------
  //-------------------------------------------

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      cssClass: 'loadData'
    });
    this.loading.present();
  }
}
