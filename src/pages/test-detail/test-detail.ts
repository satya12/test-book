// import { fetchTestService } from './../../services/fetchTest';
import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'test-detail.html'
})
export class TestDetailPage {
  fsubmitted: boolean = false;
  Form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) {
    this.createForm();
  }

  //----------------------------------------
  //-----------Validators-------------------
  //----------------------------------------

  createForm() {
    this.Form = new FormGroup({
      testName: new FormControl('', Validators.required),
      orgName: new FormControl('', Validators.required),
      class: new FormControl('', Validators.required),
      acdYear: new FormControl('', Validators.required),
      subject: new FormControl('', Validators.required),
      time: new FormControl('', Validators.required),
      tMark: new FormControl('', Validators.required)
    });
  }

  //----------------------------------------
  //-----------Submit TestDetails form------
  //----------------------------------------

  onSubmit(f) {
    if (f.valid) {
      this.storage.set('test', f.value);
      this.navCtrl.push(TabsPage);
    } else {
      this.fsubmitted = true;
    }
  }
}
