// import { TestDetailPageModule } from './test-detail.module';
import { TestDetailPage } from './test-detail';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
// import { DashboardPage } from './dashboard';

@NgModule({
  declarations: [TestDetailPage],
  imports: [IonicPageModule.forChild(TestDetailPage)]
})
export class TestDetailPageModule {}
