import { SelectedQuestionPage } from './../selected-question/selected-question';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  isChoosed: false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private events: Events // private fetchPro: fetchTestService
  ) {
    this.events.subscribe('tabToggle', data => {
      this.isChoosed = data;
      console.log(data);
    });
  }

  hmPage = HomePage;
  selectQuestion = SelectedQuestionPage;

  ionViewWillLeave() {
    this.events.unsubscribe('tabToggle');
  }
}
