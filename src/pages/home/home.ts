// import { SlidesPage } from './../slides/slides';
// import { RatingProvider } from './../../providers/rating/rating';
// import { Headers, Response } from '@angular/http';
import { fetchTestService } from './../../services/fetchTest';
import { QuesProvider } from './../../providers/ques/ques';
import { Storage } from '@ionic/storage';

import {
  Events,
  // Platform,
  MenuController,
  // PopoverController,
  // ViewController,
  // App,
  // Popover,
  Loading,
  LoadingController,
  ToastController
} from 'ionic-angular';

import {
  Component,
  // OnInit,
  ViewChild
  // trigger,
  // state,
  // style,
  // transition,
  // animate
} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
// import { AuthService } from '../../services/auth.service';
// import { PopoverPage } from '../popover/popover';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loading: Loading;
  questions: any[] = [];
  isShow = false;
  isPopover: boolean = false;
  popeOverCtrl: any;
  testDetails: any;

  selectedQuestions: any[] = [];
  search: string;
  subject: string;
  classes = true;

  tabsPage = TabsPage;
  @ViewChild('nav') nav: NavController;

  isSelected: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public event: Events,
    private quesService: QuesProvider,
    private fetchtestServie: fetchTestService,
    private menCtrl: MenuController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    this.showLoading();
    this.storage.get('test').then(test => {
      if (test) {
        this.categoryQuestion(test.subject);
      }
    });
  }

  //-------------------------------------------
  //----------Sending selected question ---
  //-------------------------------------------

  onShow(index: number) {
    this.quesService.addQuestions(this.questions[index]);
    if (this.questions.length > 0) {
      console.log(this.questions);
      this.event.publish('tabToggle', true);
    } else {
      this.event.publish('tabToggle', false);
    }
  }

  //-------------------------------------------
  //----------Category question---
  //-------------------------------------------

  categoryQuestion(category) {
    this.menCtrl.close();
    // this.fetchtestServie.testDetails.subject = category;
    this.fetchtestServie.categoryQuestions('', category).subscribe(
      res => {
        console.log(res);
        this.loading.dismiss();
        if (res.length <= 0) {
          this.presentToast(
            'The is no questions available for the selected categorie'
          );
        } else {
          this.questions = res;
        }
      },
      error => {
        console.log(error);
        this.loading.dismiss();
        this.presentToast(error);
      }
    );
  }

  load() {
    location.reload();
  }

  //-------------------------------------------
  //------------- Loader display ------------------
  //-------------------------------------------

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      cssClass: 'loadData'
    });
    this.loading.present();
  }

  //-------------------------------------------
  //----------displaying overall error msgs ---
  //-------------------------------------------

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      cssClass: 'toastStyling',
      duration: 5000,
      position: 'middle',
      showCloseButton: true,
      closeButtonText: 'X'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
