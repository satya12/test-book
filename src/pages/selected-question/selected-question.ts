import { DashboardPage } from './../dashboard/dashboard';
// import { pdfMake } from 'pdfmake/build/pdfmake';
// import { AuthService } from './../../services/auth.service';
import { fetchTestService } from './../../services/fetchTest';
// import { HomePage } from './../home/home';
import { QuesProvider } from './../../providers/ques/ques';
import { Component } from '@angular/core';
import { PopoverPage } from './../popover/popover';

import { File } from '@ionic-native/file';

import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ToastController
} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

// import pdfMake from 'pdfmake/build/pdfmake';
// import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'page-selected-question',
  templateUrl: 'selected-question.html'
})
export class SelectedQuestionPage {
  questions: any[] = [];

  text: string = '';

  selectedQuestions1: any[] = [];
  search: string;
  subject: string;
  classes = true;
  doc: any;
  testDetails: any;
  arr = [];

  constructor(
    public file: File,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    private quesService: QuesProvider,
    private fethPro: fetchTestService,
    private popoverCtrl: PopoverController,
    private storage: Storage
  ) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  iframe: any;

  ionViewDidLoad() {
    console.log('test....');
    this.storage.get('test').then(test => {
      if (test) {
        console.log('ddd', test);
        this.testDetails = test;
        this.selectedQuestions1 = this.quesService.getSelectedQuestion();
        console.log('selectedQestions Array:', this.selectedQuestions1);
        console.log('pro....', this.fethPro);
      }
    });
  }

  onCreatePdf() {
    let content = [];

    content.push(
      {
        text: this.testDetails.orgName,
        style: 'header',
        alignment: 'center'
      },
      {
        text: `Academic Year:  ${this.testDetails.acdYear}`,
        alignment: 'center',
        bold: false
      },
      {
        text: `Class:  ${this.testDetails.class}`,
        alignment: 'center',
        bold: false
      },
      {
        text: `Subject:  ${this.testDetails.subject} \n\n\n`,
        alignment: 'center',
        bold: false
      }
    );

    for (let i = 0; i < this.selectedQuestions1.length; i++) {
      let ques = {
        alignment: 'justify',
        columns: [
          {
            text: i + 1 + '.  ' + this.selectedQuestions1[i].text + '\n' + '\n'
          }
        ]
      };
      content.push(ques);
      let options = {
        columns: [
          {
            width: '*',
            text: 'a)  ' + this.selectedQuestions1[i].options[0]
          },
          {
            width: '*',
            text: 'b)  ' + this.selectedQuestions1[i].options[1]
          },
          {
            width: '*',
            text: 'c)  ' + this.selectedQuestions1[i].options[2]
          },
          {
            width: '*',
            text:
              'd)  ' +
              this.selectedQuestions1[i].options[3] +
              '\n' +
              '\n' +
              '\n'
          }
        ]
      };
      content.push(options);
    }

    var docDefinition = {
      content,
      styles: {
        header: {
          fontSize: 18,
          bold: true
        },
        bigger: {
          fontSize: 15,
          italics: true
        }
      },
      defaultStyle: {
        columnGap: 20
      }
    };

    pdfMake.createPdf(docDefinition).open({}, window);
    // pdfMake.createPdf(docDefinition).download('cfeindia_test.pdf')({}, window);;
  }

  download() {
    let self = this;
    console.log('in download function');
    let content = [];

    content.push(
      {
        text: this.testDetails.orgName,
        style: 'header',
        alignment: 'center'
      },
      {
        text: `Academic Year:  ${this.testDetails.acdYear}`,
        alignment: 'center',
        bold: false
      },
      {
        text: `Class:  ${this.testDetails.class}`,
        alignment: 'center',
        bold: false
      },
      {
        text: `Subject:  ${this.testDetails.subject} \n\n\n`,
        alignment: 'center',
        bold: false
      }
    );

    for (let i = 0; i < this.selectedQuestions1.length; i++) {
      let ques = {
        alignment: 'justify',
        columns: [
          {
            text: i + 1 + '.  ' + this.selectedQuestions1[i].text + '\n' + '\n'
          }
        ]
      };
      content.push(ques);
      let options = {
        columns: [
          {
            width: '*',
            text: 'a)  ' + this.selectedQuestions1[i].options[0]
          },
          {
            width: '*',
            text: 'b)  ' + this.selectedQuestions1[i].options[1]
          },
          {
            width: '*',
            text: 'c)  ' + this.selectedQuestions1[i].options[2]
          },
          {
            width: '*',
            text:
              'd)  ' +
              this.selectedQuestions1[i].options[3] +
              '\n' +
              '\n' +
              '\n'
          }
        ]
      };
      content.push(options);
    }

    var docDefinition = {
      content,
      styles: {
        header: {
          fontSize: 18,
          bold: true
        },
        bigger: {
          fontSize: 15,
          italics: true
        }
      },
      defaultStyle: {
        columnGap: 20
      }
    };

    pdfMake.createPdf(docDefinition).getBuffer(function(buffer) {
      let utf8 = new Uint8Array(buffer);
      let binaryArray = utf8.buffer;
      self.saveToDevice(binaryArray, 'cfeindia_test.pdf');
    });
  }
  saveToDevice(data: any, savefile: any) {
    let self = this;
    self.file.writeFile(self.file.externalDataDirectory, savefile, data, {
      replace: false
    });
    const toast = self.toastCtrl.create({
      message: 'File Has downloaded to your device',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    this.navCtrl.push(DashboardPage);
  }

  // const doc = pdfMake.createPdf(docDefinition);
  // doc.getBase64(data => {
  //   window.location.href = 'data:application/pdf;base64,' + data;
  // });

  // doc.download('abc.pdf');

  // pdfMake.createPdf(docDefinition).download('cfeindia_test.pdf');
}
// }
