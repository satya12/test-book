import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedQuestionPage } from './selected-question';

@NgModule({
  declarations: [SelectedQuestionPage],
  imports: [IonicPageModule.forChild(SelectedQuestionPage)]
})
export class SelectedQuestionPageModule {}
