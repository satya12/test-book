// import { SignupPage } from './../signup/signup';
import { DashboardPage } from './../dashboard/dashboard';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import {
  IonicPage,
  NavController,
  NavParams,
  App,
  ToastController,
  LoadingController
} from 'ionic-angular';
import { NgForm } from '@angular/forms';
// import { TabsPage } from '../tabs/tabs';
import { AuthService } from '../../services/auth.service';

import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/core';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'

  // animations: [
  //   //For the logo
  //   trigger('flyInBottomSlow', [
  //     state(
  //       'in',
  //       style({
  //         transform: 'translate3d(0,0,0)'
  //       })
  //     ),
  //     transition('void => *', [
  //       style({ transform: 'translate3d(0,2000px,0' }),
  //       animate('2000ms ease-in-out')
  //     ])
  //   ]),

  //   //For the background detail
  //   trigger('flyInBottomFast', [
  //     state(
  //       'in',
  //       style({
  //         transform: 'translate3d(0,0,0)'
  //       })
  //     ),
  //     transition('void => *', [
  //       style({ transform: 'translate3d(0,2000px,0)' }),
  //       animate('1000ms ease-in-out')
  //     ])
  //   ]),

  //   //For the login form
  //   trigger('bounceInBottom', [
  //     state(
  //       'in',
  //       style({
  //         transform: 'translate3d(0,0,0)'
  //       })
  //     ),
  //     transition('void => *', [
  //       animate(
  //         '2000ms 200ms ease-in',
  //         keyframes([
  //           style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
  //           style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
  //           style({ transform: 'translate3d(0,0,0)', offset: 1 })
  //         ])
  //       )
  //     ])
  //   ]),

  //   //For login button
  //   trigger('fadeIn', [
  //     state(
  //       'in',
  //       style({
  //         opacity: 1
  //       })
  //     ),
  //     transition('void => *', [
  //       style({ opacity: 0 }),
  //       animate('1000ms 2000ms ease-in')
  //     ])
  //   ])
  // ]
})
export class LoginPage {
  loginform: FormGroup;
  logoState: any = 'in';
  cloudState: any = 'in';
  loginState: any = 'in';
  formState: any = 'in';
  displaybutton = false;
  fsubmitted: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private app: App,
    private toast: ToastController,
    private loader: LoadingController,
    private authService: AuthService
  ) {
    this.createForm();
  }

  createForm() {
    this.loginform = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  //===============================
  //    Form submit for login
  //===============================
  onLogin(form: NgForm) {
    if (form.valid) {
      let loading = this.loader.create({
        content: 'Please wait....',
        spinner: 'circles',
        duration: 10000
      });

      loading.present();

      this.authService.logIn(form.value).subscribe(
        userData => {
          loading.dismiss();
          console.log('user', userData);
          this.authService.authSuccess(userData.token, userData.user);
          this.app.getRootNav().setRoot(DashboardPage);
        },
        err => {
          console.log(err);
          loading.dismiss();
          this.toastCreator(JSON.parse(err._body).msg);
        }
      );
    } else {
      this.fsubmitted = true;
    }
  }

  //=============================
  //    TOAST CREATION
  //=============================
  toastCreator(message: string) {
    this.toast
      .create({
        message: message,
        duration: 1500
      })
      .present();
  }
}
